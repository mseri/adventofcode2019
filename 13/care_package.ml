open Lwt.Syntax

let input =
  Utils.read_lines ~path:"input"
  |> List.hd
  |> String.split_on_char ','
  |> List.map int_of_string

type tile = Empty | Wall | Block | Paddle | Ball

let tile_of_int = function
  | 0 -> Empty
  | 1 -> Wall
  | 2 -> Block
  | 3 -> Paddle
  | 4 -> Ball
  | k -> raise (Invalid_argument (Printf.sprintf "unknown tile_id: %i" k))

module Screen = Map.Make (struct
  type t = int * int

  let compare = Stdlib.compare
end)

let seq_init n f =
  let rec next k () = if k >= n then Seq.Nil else Cons (f k, next (k + 1)) in
  next 0

let tile_to_char = function
  | Empty -> ' '
  | Wall -> '#'
  | Block -> '%'
  | Paddle -> '='
  | Ball -> 'o'

let to_canvas screen : int * char Seq.t =
  let min_x, min_y, max_x, max_y =
    Screen.to_seq screen
    |> Seq.fold_left
         (fun (min_x, min_y, max_x, max_y) ((x, y), _) ->
           (min min_x x, min min_y y, max max_x x, max max_y y))
         Int.(max_int, max_int, min_int, min_int)
  in
  let height = max_y + 1 - min_y in
  let width = max_x + 1 - min_x in

  let canvas =
    seq_init (width * height) (fun pos ->
        let x = max_x - (pos mod width) in
        let y = pos / width in
        Option.fold ~none:' ' ~some:tile_to_char (Screen.find_opt (x, y) screen))
  in

  (width, canvas)

type refresh = Not_refreshed | Refreshing | Refreshed

type ('state, 'tile) automaton = {
    initial : 'state
  ; final : 'state -> bool
  ; transition : 'tile -> 'state -> 'state
}

let refreshed =
  {
    initial = Not_refreshed
  ; final = (function Refreshed -> true | _ -> false)
  ; transition =
      (function
      | None -> ( function Not_refreshed -> Refreshing | state -> state )
      | Some _ -> ( function Refreshing -> Refreshed | state -> state ))
  }

type game = {
    screen : tile Screen.t
  ; score : int
  ; snapshot : refresh
  ; ball : int option
  ; paddle : int option
}

let game_initial =
  {
    screen = Screen.empty
  ; score = 0
  ; snapshot = refreshed.initial
  ; ball = None
  ; paddle = None
  }

let play_game state : game Lwt.t =
  let input, to_game = Lwt_stream.create () in
  let output, to_player = Lwt_stream.create () in

  let game = Intcode.vm_io state ~input ~send_out:to_player in

  let rec ai_player game : game Lwt.t =
    let closed = Lwt_stream.is_closed output in
    let* empty = Lwt_stream.is_empty output in
    if closed && empty then (
      to_game None;
      Lwt.return game )
    else
      let* x = Lwt_stream.next output in
      let* y = Lwt_stream.next output in
      let* tile_id = Lwt_stream.next output in

      let updated_score () = { game with score = tile_id } in
      let updated_game () =
        let tile = tile_of_int tile_id in
        let old_tile = Screen.find_opt (x, y) game.screen in
        let ball =
          match (tile, old_tile) with
          | Ball, _ -> Some x
          | _, Some Ball -> None
          | _ -> game.ball
        in
        let paddle =
          match (tile, old_tile) with
          | Paddle, _ -> Some x
          | _, Some Paddle -> None
          | _ -> game.paddle
        in
        let screen = Screen.add (x, y) tile game.screen in

        (* Uncomment to see how it plays!
           let width, canvas = to_canvas screen in
           Printf.printf "\n%s\n%!" (Utils.render ~width canvas);
        *)
        let snapshot =
          let snapshot = refreshed.transition ball game.snapshot in
          let game_needs_input = refreshed.final snapshot in
          match (game_needs_input, ball, paddle) with
          | true, Some x_ball, Some x_pad ->
              let joystick = Int.compare x_ball x_pad in
              to_game (Some joystick);
              refreshed.initial
          | _ -> snapshot
        in

        { screen; score = game.score; ball; paddle; snapshot }
      in

      let is_score = x = -1 && y = 0 in
      let game = if is_score then updated_score () else updated_game () in
      ai_player game
  in
  let* () = game and* game_status = ai_player game_initial in
  Lwt.return game_status

let run_cabinet_with (memory : int list) : game =
  let state = Intcode.initial_state ~memory in
  Lwt_main.run (play_game state)

let count_blocks (screen : tile Screen.t) : int =
  Screen.(cardinal (filter (fun _ tile -> tile = Block) screen))

let () =
  let memory = input in

  let screen = (run_cabinet_with memory).screen in
  let blocks_at_start = count_blocks screen in
  let hacked_memory = 2 :: List.tl memory in
  let score = (run_cabinet_with hacked_memory).score in

  Printf.printf "Part 1: %i\n" blocks_at_start;
  Printf.printf "Part 2: %i\n" score
