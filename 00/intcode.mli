type registers = { pc : int; base : int }

module IntMap : Map.S with type key = int

type t = { memory : int IntMap.t; registers : registers }

val get_cell : int IntMap.t -> int -> int

val initial_state : memory:int list -> t

val vm_io :
  t -> input:int Lwt_stream.t -> send_out:(int option -> unit) -> unit Lwt.t

val run : memory:int list -> input:int list -> int IntMap.t * int list
