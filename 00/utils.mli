val read_lines : path:string -> string list
(** Reads a file and returns it as lines *)

val int_rev : int -> int -> int
(** [int_rev k n] reverse the [k] right-most digits of [n] *)

val int_of_bool : bool -> int
(** 1 if true, 0 if false *)

val between : value:int -> int -> int -> bool

val gcd : int -> int -> int
(** [gcd a b] is the greatest common divisor of [a] and [b] *)

val lcm : int -> int -> int
(** [lcm a b] is the least common multiple of [a] and [b] *)

val range : int -> int -> int Seq.t
(** Sequence of integers between to and from, inclusive. *)

val min_fst : int * 'b -> int * 'b -> int * 'b
(** Return the tuple that has the minimum integer as first field. *)

val render : width:int -> char Seq.t -> string
(** Renders a sequence of characters into a string with lines of [width]
    characters *)
