val generate : int -> (int -> 'a) -> int -> unit -> 'a Seq.node

val head_exn : 'a Seq.t -> 'a
(** Returns the first of a sequence, or raises an exception if there is none. *)

val length : 'a Seq.t -> int
(** Returns the number of elements in the sequence. *)

val for_all : ('a -> bool) -> 'a Seq.t -> bool
(** [for_all p s] Checks if all elements in the sequence [s] satisfy the
    predicate [p]. [true] if [s] is [Nil] *)
