let lines_fold f start input =
  let accumulator = ref start in
  let running = ref true in
  while !running do
    let line = try Some (input_line input) with End_of_file -> None in
    match line with
    | Some line -> accumulator := f !accumulator line
    | None -> running := false
  done;
  !accumulator

(** open a file, and make sure the close is always done *)
let with_input_channel file f =
  let input = open_in file in
  Fun.protect ~finally:(fun () -> close_in input) (fun () -> f input)

let file_lines_fold f start file_path =
  with_input_channel file_path (lines_fold f start)

let read_lines ~(path : string) : string list =
  List.rev (file_lines_fold (fun acc line -> line :: acc) [] path)

let int_rev k n =
  let rec loop rev n = function
    | 0 -> rev
    | k -> loop ((rev * 10) + (n mod 10)) (n / 10) (k - 1)
  in
  loop 0 n k

let int_of_bool b = if b then 1 else 0

let between ~value l h = (value > l && value < h) || (value < l && value > h)

let rec gcd a = function 0 -> a | b -> gcd b (a mod b)

let lcm m n =
  match (m, n) with 0, _ | _, 0 -> 0 | m, n -> abs (m * n) / gcd m n

let range from until =
  let length, get =
    if until < from then (from - until + 1, fun x -> x + until)
    else (until - from + 1, fun x -> x + from)
  in
  Seqext.generate length get 0

let min_fst tuple tuple' = if fst tuple > fst tuple' then tuple' else tuple

let render ~width raw =
  let flat_image = String.of_seq raw in

  let height =
    Float.(ceil (of_int (String.length flat_image) /. of_int width) |> to_int)
  in
  let rows = range (height - 1) 0 in

  let matrix_image =
    Seq.map (fun i -> String.sub flat_image (i * width) width) rows
  in

  matrix_image |> List.of_seq |> String.concat "\n"
