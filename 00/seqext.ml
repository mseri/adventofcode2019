open Stdlib.Seq

let rec generate length get k () =
  if k = length then Nil else Cons (get k, generate length get (k + 1))

let head_exn s = match s () with Nil -> raise Not_found | Cons (x, _) -> x

let length s = fold_left (fun acc _ -> acc + 1) 0 s

let rec for_all p s =
  match s () with
  | Nil -> true
  | Cons (x, _) when not (p x) -> false
  | Cons (_, s) -> for_all p s
