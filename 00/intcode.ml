open Lwt.Syntax

let debug = ref false

type reason = UnknownOpCode of int | UnknownMode of char

exception RuntimeFailure of reason

type registers = { pc : int; base : int }

module IntMap = Map.Make (Int)

type t = { memory : int IntMap.t; registers : registers }

let debug_print msg = if !debug then Lwt_io.printl msg else Lwt.return_unit

let get_cell memory position =
  match IntMap.find_opt position memory with Some value -> value | None -> 0

let read_value state value = function
  | '0' -> get_cell state.memory value (* absolute *)
  | '1' -> value (* immediate *)
  | '2' -> get_cell state.memory (state.registers.base + value) (* relative *)
  | mode -> raise @@ RuntimeFailure (UnknownMode mode)

let write_value state value = function
  | '0' -> value (* immediate *)
  | '2' -> state.registers.base + value (* relative *)
  | mode -> raise @@ RuntimeFailure (UnknownMode mode)

let update (memory : int IntMap.t) address value =
  IntMap.add address value memory

let rec step (state : t) (input : int Lwt_stream.t)
    (send_out : int option -> unit) : t Lwt.t =
  let pc, base = (state.registers.pc, state.registers.base) in
  let pc_value = get_cell state.memory pc in
  let op = pc_value mod 100 in
  let mode_raw = pc_value / 100 |> Utils.int_rev 3 |> string_of_int in
  let mode =
    match String.length mode_raw with
    | 1 -> "00" ^ mode_raw
    | 2 -> "0" ^ mode_raw
    | _ -> mode_raw
  in
  let read_cell value mode = read_value state value mode in
  let write_cell value mode = write_value state value mode in
  let get_op_mode pos =
    let op = get_cell state.memory (pc + pos + 1) in
    let mode = mode.[pos] in
    (op, mode)
  in

  let msg = Printf.sprintf "Running op:%i\tmode:%s pc:%i" op mode pc in
  let* () = debug_print msg in

  let arithmetic (func : int -> int -> int) =
    let op_left, mode_left = get_op_mode 0 in
    let op_right, mode_right = get_op_mode 1 in
    let op_target, mode_target = get_op_mode 2 in
    let left = read_cell op_left mode_left in
    let right = read_cell op_right mode_right in
    let target = write_cell op_target mode_target in
    let memory = update state.memory target (func left right) in
    let registers = { pc = pc + 4; base } in
    step { memory; registers } input send_out
  in
  let read () =
    let op_value, mode = get_op_mode 0 in
    let target = write_cell op_value mode in
    let* in_value = Lwt_stream.next input in
    let memory = update state.memory target in_value in
    let registers = { pc = pc + 2; base } in
    step { memory; registers } input send_out
  in
  let write () =
    let op_value, mode = get_op_mode 0 in
    let out_value = read_cell op_value mode in
    send_out @@ Some out_value;
    let registers = { pc = pc + 2; base } in
    step { state with registers } input send_out
  in
  let jump comp =
    let op_left, mode_left = get_op_mode 0 in
    let op_right, mode_right = get_op_mode 1 in
    let left = read_cell op_left mode_left in
    let right = read_cell op_right mode_right in
    let next = if comp left then right else pc + 3 in
    let registers = { pc = next; base } in
    step { state with registers } input send_out
  in
  let boolean comp = arithmetic (fun a b -> Utils.int_of_bool @@ comp a b) in
  let adjust_relative_base () =
    let op_value, mode = get_op_mode 0 in
    let base_offset = read_cell op_value mode in
    let registers = { pc = pc + 2; base = base + base_offset } in
    step { state with registers } input send_out
  in
  match op with
  | 99 ->
      send_out None;
      Lwt.return state
  | 1 -> arithmetic ( + )
  | 2 -> arithmetic ( * )
  | 3 -> read ()
  | 4 -> write ()
  | 5 -> jump (( <> ) 0)
  | 6 -> jump (( = ) 0)
  | 7 -> boolean ( < )
  | 8 -> boolean ( = )
  | 9 -> adjust_relative_base ()
  | op -> Lwt.fail @@ RuntimeFailure (UnknownOpCode op)

let memory_of_list mem =
  mem |> List.mapi (fun i x -> (i, x)) |> List.to_seq |> IntMap.of_seq

let initial_state ~memory =
  { memory = memory_of_list memory; registers = { pc = 0; base = 0 } }

let vm_io state ~input ~send_out =
  let* _state = step state input send_out in
  Lwt.return_unit

let run ~memory ~input =
  let vm =
    let input = Lwt_stream.of_list input in
    let out, send_out = Lwt_stream.create () in
    let* state = step (initial_state ~memory) input send_out in
    let* output = Lwt_stream.to_list out in
    Lwt.return (state.memory, output)
  in
  Lwt_main.run vm
