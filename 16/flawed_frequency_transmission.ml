let input =
  Utils.read_lines ~path:"input"
  |> List.hd
  |> String.to_seq
  |> Seq.map (fun c -> int_of_char c - 48)
  |> Array.of_seq

let sum_of_ranged_map f start stop signal =
  let sum = ref 0 in
  for j = start to stop do
    sum := !sum + f signal.(j)
  done;
  !sum

let sum_row stop signal i : int =
  let rec sum_block sum j k =
    if j > stop then sum
    else
      let until = min (j + i) stop in
      let next = j + (2 * (i + 1)) in
      let element = sum_of_ranged_map (( * ) k) j until signal in
      sum_block (sum + element) next (k * -1)
  in
  sum_block 0 i 1

let fft signal =
  let length = Array.length signal in
  Array.init length (fun i ->
      let row = sum_row (length - 1) signal i |> string_of_int in
      row.[String.length row - 1] |> int_of_char |> ( + ) (-48))

let fast_fft signal =
  let sum = ref (sum_of_ranged_map Fun.id 0 (Array.length signal - 1) signal) in
  let element i =
    let elem = abs !sum mod 10 in
    sum := !sum - signal.(i);
    elem
  in
  Array.(init (length signal) element)

let rec repeat f result = function
  | k when k < 1 -> result
  | k -> repeat f (f result) (k - 1)

let take_from_signal n signal =
  String.init
    (min n (Array.length signal))
    (fun i -> char_of_int (signal.(i) + 48))

let () =
  let output = repeat fft input 100 in
  let message = take_from_signal 8 output in

  let real_signal =
    Array.(
      init
        ((length input * 10000) - 7)
        (fun i -> input.((i + 7) mod length input)))
  in
  let index = take_from_signal 7 input |> int_of_string |> ( + ) (-7) in
  let manageable_signal =
    Array.(init (length real_signal - index) (fun i -> real_signal.(i + index)))
  in
  let real_output = repeat fast_fft manageable_signal 100 in
  let real_message = take_from_signal 8 real_output in

  Printf.printf "Part 1: %s\n" message;
  Printf.printf "Part 2: %s\n" real_message
