let input =
  Utils.read_lines ~path:"input"
  |> List.hd
  |> String.split_on_char ','
  |> List.map int_of_string

let run noun verb memory =
  let memory =
    match memory with
    | op :: _ :: _ :: ops -> op :: noun :: verb :: ops
    | _ -> raise (Invalid_argument "Memory is too small")
  in
  fst (Intcode.run ~memory ~input:[])

let () =
  let first_opcode = Intcode.get_cell (run 12 2 input) 0 in

  let nouns = Utils.range 0 99 in
  let verbs = Utils.range 0 99 in

  let returns expected (noun, verb) =
    match Intcode.get_cell (run noun verb input) 0 with
    | result when result = expected -> Some ((100 * noun) + verb)
    | _ -> None
  in
  let winner =
    nouns
    |> Seq.flat_map (fun noun -> verbs |> Seq.map (fun verb -> (noun, verb)))
    |> Seq.filter_map (returns 19690720)
    |> Seqext.head_exn
  in

  Printf.printf "Part 1: %u\n" first_opcode;
  Printf.printf "Part 2: %u\n" winner
