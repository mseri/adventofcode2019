let input = Utils.read_lines ~path:"input"

let heigth = List.length input

let width = String.length (List.nth input 0)

let asteroids = Array.make_matrix heigth width false

let _ =
  input
  |> List.mapi @@ fun y line ->
     line
     |> String.iteri @@ fun x c ->
        match c with '#' -> asteroids.(y).(x) <- true | _ -> ()

let coord yx =
  let y = yx / width in
  let x = yx mod width in
  (y, x)

let have_direct_sight y x y' x' =
  let dx = x' - x in
  let dy = y' - y in
  match (abs dx, abs dy, Utils.gcd dx dy) with
  | 0, 0, _ -> false
  | 0, 1, _ | 1, 0, _ | _, _, 1 | _, _, -1 -> true
  | _, _, gcd ->
      Utils.range (compare gcd 0) (gcd - compare gcd 0)
      |> Seqext.for_all (fun factor ->
             not asteroids.(y + (factor * dy / gcd)).(x + (factor * dx / gcd)))

let with_direct_sight y x yx' =
  let y', x' = coord yx' in
  asteroids.(y').(x') && have_direct_sight y x y' x'

let count_direct_sight yx =
  let y, x = coord yx in

  if not asteroids.(y).(x) then 0
  else
    Utils.range 0 ((heigth * width) - 1)
    |> Seq.filter (with_direct_sight y x)
    |> Seqext.length

let () =
  let best =
    Utils.range 0 ((heigth * width) - 1)
    |> Seq.map count_direct_sight
    |> Seq.fold_left max Int.min_int
  in

  Printf.printf "Part 1: %u\n" best
