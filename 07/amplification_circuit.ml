open! Lwt.Syntax

let input =
  Utils.read_lines ~path:"input"
  |> List.hd
  |> String.split_on_char ','
  |> List.map int_of_string

(* interleave 1 [2;3] = [ [1;2;3]; [2;1;3]; [2;3;1] ] *)
let rec interleave x = function
  | [] -> [ [ x ] ]
  | hd :: tl as lst ->
      (x :: lst) :: List.map (fun y -> hd :: y) (interleave x tl)

let rec permutations = function
  | hd :: tl -> List.concat (List.map (interleave hd) (permutations tl))
  | lst -> [ lst ]

let loop_comms channels =
  let rec loop acc = function
    | [] -> []
    | [ channel ] -> channel :: List.rev acc
    | channel :: channels -> loop (channel :: acc) channels
  in
  loop [] channels

let amps_to_eleven state phases input : int Lwt.t =
  (* Set up network of amps *)
  let channels, to_channels =
    List.fold_left
      (fun (input, send_in) _ ->
        let out, send_out = Lwt_stream.create () in
        (out :: input, send_out :: send_in))
      ([], []) phases
  in
  let channels = loop_comms channels in
  let output = List.hd channels in

  let amps =
    List.map2
      (fun input send_out -> Intcode.vm_io state ~input ~send_out)
      channels to_channels
  in

  (* Populate comms *)
  List.iter2 (fun phase send_in -> send_in @@ Some phase) phases to_channels;
  (to_channels |> List.rev |> List.hd |> fun send_in -> send_in @@ Some input);

  (* Beep boot *)
  let wired_amps : int Lwt.t =
    let* () = Lwt.join amps in
    let* () = Lwt_stream.closed output in
    let* output = Lwt_stream.next output in
    Lwt.return output
  in
  wired_amps

let run_amplifier (memory : int list) (phases : int list) : int =
  let state = Intcode.initial_state ~memory in
  Lwt_main.run @@ amps_to_eleven state phases 0

let get_best = List.fold_left max Int.min_int

let () =
  let memory = input in
  let simple_phases = permutations [ 0; 1; 2; 3; 4 ] in

  let looped_phases = permutations [ 5; 6; 7; 8; 9 ] in

  let pick_in phases = phases |> List.map (run_amplifier memory) |> get_best in

  let the_best = pick_in simple_phases in
  let the_best_looped = pick_in looped_phases in

  Printf.printf "Part 1: %i\n" the_best;
  Printf.printf "Part 2: %i\n" the_best_looped
