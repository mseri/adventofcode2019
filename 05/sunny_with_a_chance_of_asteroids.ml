let input =
  Utils.read_lines ~path:"input"
  |> List.hd
  |> String.split_on_char ','
  |> List.map int_of_string

let () =
  let memory = input in
  let _, out_1 = Intcode.run ~memory ~input:[ 1 ] in
  let _, out_2 = Intcode.run ~memory ~input:[ 5 ] in

  Printf.printf "Part 1: %u\n" (List.hd @@ List.rev out_1);
  Printf.printf "Part 2: %u\n" (List.hd @@ List.rev out_2)
