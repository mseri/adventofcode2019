open Lwt.Syntax

let input =
  Utils.read_lines ~path:"input"
  |> List.hd
  |> String.split_on_char ','
  |> List.map int_of_string

module HullMap = Map.Make (struct
  type t = int * int

  let compare = Stdlib.compare
end)

let registration_identifier_painter state start : int HullMap.t Lwt.t =
  let input, to_brain = Lwt_stream.create () in
  let output, to_bot = Lwt_stream.create () in

  to_brain (Some start);

  let brain = Intcode.vm_io state ~input ~send_out:to_bot in

  let hull = HullMap.empty in
  let position = (0, 0) in
  let step = (1, 0) in

  let rec emergency_robot hull position step : int HullMap.t Lwt.t =
    let closed = Lwt_stream.is_closed output in
    let* empty = Lwt_stream.is_empty output in
    if closed && empty then (
      to_brain None;
      Lwt.return hull )
    else
      let* colour = Lwt_stream.next output in
      let* direction = Lwt_stream.next output in

      let hull = HullMap.add position colour hull in

      let step =
        match direction with
        | 0 -> (~-1 * snd step, fst step)
        | 1 -> (snd step, ~-1 * fst step)
        | _ -> raise (Failure "Bot is drunk.")
      in

      let position = (fst position + fst step, snd position + snd step) in

      to_brain (Some (Option.value ~default:0 (HullMap.find_opt position hull)));

      emergency_robot hull position step
  in
  let* () = brain and* hull = emergency_robot hull position step in
  Lwt.return hull

let run_spray start_colour (memory : int list) : int HullMap.t =
  let state = Intcode.initial_state ~memory in
  Lwt_main.run (registration_identifier_painter state start_colour)

let seq_init n f =
  let rec next k () = if k >= n then Seq.Nil else Cons (f k, next (k + 1)) in
  next 0

let to_canvas hull =
  let min_x, min_y, max_x, max_y =
    HullMap.to_seq hull
    |> Seq.fold_left
         (fun (min_x, min_y, max_x, max_y) ((x, y), _) ->
           (min min_x x, min min_y y, max max_x x, max max_y y))
         Int.(max_int, max_int, min_int, min_int)
  in
  let width = max_y + 1 - min_y in
  let height = max_x + 1 - min_x in

  let canvas =
    seq_init (height * width) (fun pos ->
        let x = max_x - (pos / width) in
        let y = max_y - (pos mod width) in
        match HullMap.find_opt (x, y) hull with Some 1 -> '#' | _ -> ' ')
  in

  (width, canvas)

let () =
  let memory = input in
  let black_sprayed = run_spray 0 memory in
  let white_sprayed = run_spray 1 memory in

  let width, canvas = to_canvas white_sprayed in

  Printf.printf "Part 1: %i\n" (HullMap.cardinal black_sprayed);
  Printf.printf "Part 2:\n%s\n" (Utils.render ~width canvas)
