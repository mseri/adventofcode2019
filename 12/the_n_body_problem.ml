let input =
  Utils.read_lines ~path:"input"
  |> List.map (fun l ->
         Scanf.sscanf l "<x=%i, y=%i, z=%i>" (fun x y z -> (x, y, z)))

let rec repeat n f arg = if n <= 0 then arg else repeat (n - 1) f (f arg)

let add (x0, y0, z0) (x1, y1, z1) = (x0 + x1, y0 + y1, z0 + z1)

let step axis_filter (positions, velocities) =
  let gravitate m1 m2 =
    match Int.compare m1 m2 with 0 -> 0 | c when c < 0 -> -1 | _ -> 1
  in
  let apply_gravity (x1, y1, z1) (x2, y2, z2) =
    (gravitate x2 x1, gravitate y2 y1, gravitate z2 z1)
  in

  let get_forces position =
    List.fold_left
      (fun force other ->
        add (apply_gravity position other) force |> axis_filter)
      (0, 0, 0) positions
  in

  let gravities = List.map get_forces positions in
  let velocities = List.map2 add velocities gravities in
  let positions = List.map2 add positions velocities in

  (positions, velocities)

let total_energy positions velocities =
  let energy_of = List.map (fun (x, y, z) -> Int.(abs x + abs y + abs z)) in

  List.fold_left2
    (fun sum potential kinetic -> sum + (potential * kinetic))
    0 (energy_of positions) (energy_of velocities)

let total_cycle_finder initial_state =
  let cycle_finder filter =
    let step = step filter in
    let rec loop k state =
      if state = initial_state then k else loop (k + 1) (step state)
    in
    loop 1 (step initial_state)
  in

  let cycle_x = cycle_finder (fun (x, _, _) -> (x, 0, 0)) in
  let cycle_y = cycle_finder (fun (_, y, _) -> (0, y, 0)) in
  let cycle_z = cycle_finder (fun (_, _, z) -> (0, 0, z)) in

  Utils.(lcm cycle_x (lcm cycle_y cycle_z))

let () =
  let initial_state =
    (input, List.init (List.length input) (fun _ -> (0, 0, 0)))
  in

  let positions, velocities = repeat 1000 (step Fun.id) initial_state in
  let energy = total_energy positions velocities in

  let cycle = total_cycle_finder initial_state in

  Printf.printf "Part 1: %i\n" energy;
  Printf.printf "Part 2: %i\n" cycle
