let input =
  Utils.read_lines ~path:"input" |> List.hd |> fun line ->
  Scanf.sscanf line "%i-%i" (fun x y -> (x, y))

let sorted arr =
  let rec loop prev = function
    | 6 -> true
    | i ->
        let curr = arr.(i) in
        if curr < prev then false else loop curr (i + 1)
  in
  loop ~-1 0

let repeating arr =
  let rec loop prev = function
    | 6 -> false
    | i ->
        let curr = arr.(i) in
        if curr = prev then true else loop curr (i + 1)
  in
  loop ~-1 0

let repeating_twice arr =
  let rec loop prev matched = function
    | 6 -> matched = 1
    | i -> (
        let curr = arr.(i) in
        match (curr = prev, matched) with
        | false, 1 -> true
        | true, _ -> loop curr (matched + 1) (i + 1)
        | false, _ -> loop curr 0 (i + 1) )
  in
  loop ~-1 0 0

let () =
  let inputs = match input with from, until -> Utils.range from until in
  let possible_passwords =
    inputs
    |> Seq.map (fun possible ->
           possible
           |> string_of_int
           |> String.to_seq
           |> Seq.map int_of_char
           |> Array.of_seq)
  in
  let cool_passwords =
    Seq.filter (fun arr -> sorted arr && repeating arr) possible_passwords
    |> List.of_seq
  in
  let super_cool_passwords = List.filter repeating_twice cool_passwords in

  Printf.printf "Part 1: %u\n" (List.length cool_passwords);
  Printf.printf "Part 2: %u\n" (List.length super_cool_passwords)
