module C = Stdlib.Complex

type point = C.t

type segment = point * point * float

let ( +++ ) = C.add

let ( --- ) = C.sub

let ( *** ) = C.mul

let input =
  let to_move c k =
    match c with
    | 'R' -> { C.re = k; im = 0. }
    | 'U' -> { C.re = 0.; im = k }
    | 'L' -> { C.re = -.k; im = 0. }
    | 'D' -> { C.re = 0.; im = -.k }
    | _ -> raise Exit
  in

  Utils.read_lines ~path:"input"
  |> List.map (fun line ->
         String.split_on_char ',' line
         |> List.map (fun s -> Scanf.sscanf s "%c%f" to_move))

let step (segments, start, travelled) move : segment list * point * float =
  let fin = start +++ move in
  ((start, fin, travelled) :: segments, fin, travelled +. C.norm move)

let walk moves =
  let paths, _end_point, _travelled =
    List.fold_left step ([], C.zero, 0.) moves
  in
  paths

let projection a b c d : float =
  let e = b --- a in
  let f = d --- c in
  let p = { C.re = -.e.im; im = e.re } in

  let projection = (f.re *. p.re) +. (f.im *. p.im) in
  if projection = 0. then Float.nan
  else (((a.re -. c.re) *. p.re) +. ((a.im -. c.im) *. p.im)) /. projection

let intersection (a, b, d1) (c, d, d2) : (point * float) option =
  let h1 = projection a b c d in
  let h2 = projection c d a b in
  let parallel = Float.is_nan h1 || Float.is_nan h2 in

  let f = d --- c in
  if (not parallel) && h1 > 0. && h1 < 1. && h2 > 0. && h2 < 1. then
    let i = c +++ { C.re = f.re *. h1; im = f.im *. h1 } in
    let delay = C.norm (i --- a) +. d1 +. (C.norm (i --- c) +. d2) in
    Some (i, delay)
  else None

let find_intersections path_a path_b : (point * float) list =
  List.concat_map
    (fun segment_a ->
      List.filter_map (fun segment_b -> intersection segment_a segment_b) path_b)
    path_a

let () =
  let a, b = match input with [ a; b ] -> (a, b) | _ -> raise Exit in

  let paths_a = walk a in
  let paths_b = walk b in

  let crossings = find_intersections paths_a paths_b in

  let closest_to_center =
    List.fold_left
      (fun acc ({ C.re = x; im = y }, _) -> Float.(min acc (abs x +. abs y)))
      Float.max_float crossings
    |> Int.of_float
  in

  let lowest_delay =
    List.fold_left (fun acc (_, d) -> Float.min acc d) Float.max_float crossings
    |> Int.of_float
  in

  Printf.printf "Part 1: %u\n" closest_to_center;
  Printf.printf "Part 2: %u\n" lowest_delay
