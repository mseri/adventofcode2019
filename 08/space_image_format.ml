let width = 25

let height = 6

let layer_length = width * height

let input = Utils.read_lines ~path:"input" |> List.hd

let n_layers = String.length input / layer_length

let layers = Array.make_matrix n_layers layer_length '0'

let () =
  input
  |> String.to_seqi
  |> Seq.iter (fun (i, c) ->
         let x = i / layer_length in
         let y = i mod layer_length in
         layers.(x).(y) <- c)

let score_layer layer =
  let working_layer = Array.copy layer in
  let zeroes, ones, twos =
    Array.fold_left
      (fun acc elt ->
        match (acc, elt) with
        | (z, o, t), '0' -> (z + 1, o, t)
        | (z, o, t), '1' -> (z, o + 1, t)
        | (z, o, t), '2' -> (z, o, t + 1)
        | _ -> acc)
      (0, 0, 0) working_layer
  in
  (zeroes, ones * twos)

let to_raw layers =
  let rows = Utils.range (Array.length layers - 1) 0 in
  let cols = Utils.range 0 (Array.length layers.(0) - 1) in
  Seq.map
    (fun j ->
      Seq.fold_left
        (fun acc i ->
          match (acc, layers.(i).(j)) with
          | '2', '2' -> '2'
          | '2', '0' -> ' '
          | '2', '1' -> '#'
          | acc, _ -> acc)
        '2' rows)
    cols

let () =
  let layer_scores = Array.map score_layer layers in
  let best = Array.fold_left Utils.min_fst (layer_length, 0) layer_scores in
  let image = to_raw layers |> Utils.render ~width in

  Printf.printf "Part 1: %u\n" (snd best);
  Printf.printf "Part 2:\n%s\n" image
